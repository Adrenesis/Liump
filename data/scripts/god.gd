extends Node
const BASE_DAMAGE = 150
const START_DELAY = 10
const GAME_OVER_DELAY = 20
const BASE_SPAWN_DELAY = 72
const START_FADE_SPEED = 0.17
onready var new_game = preload("res://data/scenes/Game.tscn")
onready var ump_furnace = preload("res://data/scenes/characters/UmpFurnace.tscn")
onready var root = get_node("/root")
onready var game = get_node("/root/Game")
onready var li = get_node("/root/Game/Ground/Li")
onready var ground_layer = get_node("/root/Game/Ground")
onready var groundbox = get_node("/root/Game/Ground/Ground/Groundbox")
onready var bbox_left = get_node("/root/Game/Ground/LeftBoundBorder")
onready var bbox_right = get_node("/root/Game/Ground/RightBoundBorder")
onready var borderbox = get_node("/root/Game/Ground/ScreenBorders/Borderbox")
onready var HpBar = get_node("/root/Game/HudLayer/HudFrame/MarginContainer/HpBar")
onready var TBar = get_node("/root/Game/HudLayer/HudFrame/MarginContainer2/TFrame")
onready var score_text = get_node("/root/Game/HudLayer/HudFrame/ScoreValueMargin/Label")
onready var combo_text = get_node("/root/Game/HudLayer/HudFrame/ComboValueMargin/Label")
onready var control_manual = get_node("/root/Game/HudLayer/HudFrame/ControlManual")
onready var press_to_start = get_node("/root/Game/HudLayer/HudFrame/PressToStart")
onready var game_over = get_node("/root/Game/HudLayer/HudFrame/GameOver")
onready var game_over_score = get_node("/root/Game/HudLayer/HudFrame/GameOverScore")
onready var you_died = get_node("/root/Game/HudLayer/HudFrame/YouDied")
onready var you_died_but = get_node("/root/Game/HudLayer/HudFrame/But")
onready var grounddict = { 
	-17: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock18"),
	-16: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock17"),
	-15: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock16"),
	-14: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock15"),
	-13: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock14"),
	-12: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock13"),
	-11: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock12"),
	-10: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock11"),
	-9: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock10"),
	-8: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock9"),
	-7: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock8"),
	-6: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock7"),
	-5: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock6"),
	-4: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock5"),
	-3: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock4"),
	-2: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock3"),
	-1: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock2"),
	0: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock"),
	1: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock19"),
	2: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock20"),
	3: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock21"),
	4: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock22"),
	5: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock23"),
	6: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock24"),
	7: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock25"),
	8: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock26"),
	9: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock27"),
	10: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock28"),
	11: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock29"),
	12: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock30"),
	13: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock31"),
	14: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock32"),
	15: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock33"),
	16: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock34"),
	17: get_node("/root/Game/Ground/Ground/Groundbox/GroundBlock35")}
# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var general_temperature
var spawn_delay
var spawn_number
var last_ump_furnace
var score
var combo
var combo_gauge
var last_combo
var last_score
var game_stopped
var displaying_controls
var displaying_game_over
var color_direction
var first_start
var frame_delay

func _ready():
	self.first_start = true
	on_game_start()
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func display_controls(value):
	control_manual.visible = value
	press_to_start.visible = value
	self.displaying_controls = value

func on_game_start():
	self.frame_delay = self.START_DELAY
	if not self.first_start:
		prepare_new_game()
	self.game_stopped = false
	self.displaying_controls = false
	self.color_direction = -1
	self.combo = 0
	self.combo_gauge = 0
	self.score = 0
	combo_text.text = str(self.combo)
	score_text.text = str(self.score)
	self.spawn_number = 0
	self.spawn_delay = self.BASE_SPAWN_DELAY
	self.general_temperature = 0
	set_process(true)
	yield(get_tree(), "idle_frame")
	stop_game()
	

func prepare_new_game():
	game = new_game.instance()
	li = game.get_node("Ground/Li")
	ground_layer = game.get_node("Ground")
	groundbox = game.get_node("Ground/Ground/Groundbox")
	borderbox = game.get_node("Ground/ScreenBorders/Borderbox")
	HpBar = game.get_node("HudLayer/HudFrame/MarginContainer/HpBar")
	TBar = game.get_node("HudLayer/HudFrame/MarginContainer2/TFrame")
	score_text = game.get_node("HudLayer/HudFrame/ScoreValueMargin/Label")
	combo_text = game.get_node("HudLayer/HudFrame/ComboValueMargin/Label")
	bbox_left = game.get_node("Ground/LeftBoundBorder")
	bbox_right = game.get_node("Ground/RightBoundBorder")
	control_manual = game.get_node("HudLayer/HudFrame/ControlManual")
	press_to_start = game.get_node("HudLayer/HudFrame/PressToStart")
	game_over = game.get_node("HudLayer/HudFrame/GameOver")
	game_over_score = game.get_node("HudLayer/HudFrame/GameOverScore")
	you_died = game.get_node("HudLayer/HudFrame/YouDied")
	you_died_but = game.get_node("HudLayer/HudFrame/But")
	grounddict = { 
	-17: groundbox.get_node("GroundBlock18"),
	-16: groundbox.get_node("GroundBlock17"),
	-15: groundbox.get_node("GroundBlock16"),
	-14: groundbox.get_node("GroundBlock15"),
	-13: groundbox.get_node("GroundBlock14"),
	-12: groundbox.get_node("GroundBlock13"),
	-11: groundbox.get_node("GroundBlock12"),
	-10: groundbox.get_node("GroundBlock11"),
	-9: groundbox.get_node("GroundBlock10"),
	-8: groundbox.get_node("GroundBlock9"),
	-7: groundbox.get_node("GroundBlock8"),
	-6: groundbox.get_node("GroundBlock7"),
	-5: groundbox.get_node("GroundBlock6"),
	-4: groundbox.get_node("GroundBlock5"),
	-3: groundbox.get_node("GroundBlock4"),
	-2: groundbox.get_node("GroundBlock3"),
	-1: groundbox.get_node("GroundBlock2"),
	0: groundbox.get_node("GroundBlock"),
	1: groundbox.get_node("GroundBlock19"),
	2: groundbox.get_node("GroundBlock20"),
	3: groundbox.get_node("GroundBlock21"),
	4: groundbox.get_node("GroundBlock22"),
	5: groundbox.get_node("GroundBlock23"),
	6: groundbox.get_node("GroundBlock24"),
	7: groundbox.get_node("GroundBlock25"),
	8: groundbox.get_node("GroundBlock26"),
	9: groundbox.get_node("GroundBlock27"),
	10: groundbox.get_node("GroundBlock28"),
	11: groundbox.get_node("GroundBlock29"),
	12: groundbox.get_node("GroundBlock30"),
	13: groundbox.get_node("GroundBlock31"),
	14: groundbox.get_node("GroundBlock32"),
	15: groundbox.get_node("GroundBlock33"),
	16: groundbox.get_node("GroundBlock34"),
	17: groundbox.get_node("GroundBlock35")}
	game.name = "Game"

func display_game_over(value):
	game_over.visible = value
	game_over_score.visible =  value
	you_died.visible = value
	you_died_but.visible =  value
	self.displaying_game_over = value

func on_game_over():
	stop_game()
	display_controls(false)
	var score_text = str("SCORE: %8d" % [ self.score ]) 
	game_over_score.text = str(score_text)
	display_game_over(true)
	self.frame_delay = self.GAME_OVER_DELAY


#


func start_game():
	display_controls(false)
	self.game_stopped = false
	for i in self.ground_layer.get_children():
		i.set_process(true)

func stop_game():
	self.game_stopped = true
	for i in self.ground_layer.get_children():
		i.set_process(false)
	display_game_over(false)
	display_controls(true)

func reset_game():
	display_game_over(false)
	root.remove_child(game)
	game.queue_free()
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	prepare_new_game()
	root.add_child(game)
	on_game_start()

func heat(groundblock):
	groundblock.hp -= self.BASE_DAMAGE * (self.general_temperature + 1)

func get_three_closest_block(x):
	var index = round(round(x-200)/12)
	var r = []
	if grounddict.has(int(index-1)):
		r.append(grounddict[int(index-1)])
	if grounddict.has(int(index)):
		r.append(grounddict[int(index)])
	if grounddict.has(int(index+1)):
		r.append(grounddict[int(index+1)])
	return r

func _process(delta):
	if self.combo != self.last_combo:
		combo_text.text = str(self.combo)
	if self.score != self.last_score:
		score_text.text = str(self.score)
	if not self.game_stopped:
		if self.combo_gauge >= 5:
			
			self.combo_gauge -= 5
			self.general_temperature -= 1
			if self.general_temperature < 0:
				self.general_temperature = 0
			else:
				li.combo_full_sfx.play(0)
		if self.spawn_delay > 0:
			self.spawn_delay -= 1
		elif self.spawn_delay == 0:
			self.spawn_delay = self.BASE_SPAWN_DELAY / (self.spawn_number+1) + 120
			self.last_ump_furnace = self.ump_furnace.instance()
			self.ground_layer.add_child(self.last_ump_furnace)
			self.last_ump_furnace.position = Vector2(randi()%401+1, 0)
			self.spawn_number += 1
		self.HpBar.value = (self.li.hp * 100.0) / self.li.MAX_HP
		self.TBar.value = self.general_temperature
		self.last_combo = self.combo
		self.last_score = self.score
	elif self.displaying_controls:
		if self.frame_delay <= 0:
			if Input.is_action_pressed("action_antiair") ||\
				Input.is_action_pressed("action_slide") ||\
				Input.is_action_pressed("action_left") ||\
				Input.is_action_pressed("action_right") ||\
				Input.is_action_pressed("action_jump"):
				start_game()
		else:
			self.frame_delay -= 1
		var calculated_color = press_to_start.modulate.a + self.START_FADE_SPEED * self.color_direction
		if calculated_color <= 0 || calculated_color >= 1:
			self.color_direction *= -1
		press_to_start.modulate.a = calculated_color
	elif self.displaying_game_over:
		if self.frame_delay <= 0:
			if Input.is_action_pressed("action_antiair") ||\
				Input.is_action_pressed("action_slide") ||\
				Input.is_action_pressed("action_left") ||\
				Input.is_action_pressed("action_right") ||\
				Input.is_action_pressed("action_jump"):
				reset_game()
			else:
				var calculated_color = you_died.modulate.a + self.START_FADE_SPEED * self.color_direction
				if calculated_color <= 0 || calculated_color >= 1:
					self.color_direction *= -1
				you_died.modulate.a = calculated_color
				you_died_but.modulate.a = 1-calculated_color
		else:
			self.frame_delay -= 1
	pass
