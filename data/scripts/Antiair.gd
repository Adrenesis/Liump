extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	self.connect("visibility_changed", self, "on_visible")
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func on_visible():
	if self.visible:
		set_process(true)
	else:
		set_process(false)
	
func _process(delta):
	if self.frame == 8:
		self.frame = 0
	else:
		self.frame +=1
	
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass

func _draw():
	self.position = Vector2(round(position.x), round(position.y))