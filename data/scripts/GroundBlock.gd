extends CollisionShape2D
const MAX_HP = 8192
var hp
var last_hp
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	get_node("Selfbox").connect("area_entered", self, "on_hurt")
	hp = sin(self.position.x/75)*1000+8900
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func on_hurt(a):
	god.heat(self)

func _process(delta):
	if self.hp < 0:
		self.hp = 0
	if (self.hp != self.last_hp):
		self.scale = Vector2(1, (self.hp * 1.0 / self.MAX_HP))
	self.last_hp = self.hp
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
