extends Node2D

const BASE_FALL_SPEED = Vector2(-0.25, 1.5)
const EJECT_SPEED = Vector2(-12, -6)
const ATTACK_MOVE_SPEED = Vector2(3,0)
const THROW_DELAY = 10
const ATTACK_DELAY = 50
const ACTIVE_DELAY = 16
const HURT_DELAY = 2
enum STATE{
	Fall,
	Idle,
	Attack,
	Ejected}
var state
var direction
var fall_speed
var attack_move_speed
var frame_delay
onready var li = god.li
onready var screen_borders = god.borderbox
onready var ground = god.groundbox
onready var timer = get_node("Timer")
onready var ump = get_node("Ump")
onready var ump_hitbox = get_node("Ump/Hitbox")
onready var ump_feetbox = get_node("Ump/Feetbox")
onready var furnace = get_node("Furnace")
onready var furnace_script = preload("res://data/scripts/Furnace.gd")
onready var graph_falling = preload("res://data/graphics/ump_falling.png")
onready var graph_calling = preload("res://data/graphics/ump_calling.png")
onready var graph_attacking = preload("res://data/graphics/ump_attacking.png")
onready var graph_ejected = preload("res://data/graphics/ump_ejected.png")


func _ready():
	refresh_direction()
	ump_hitbox.connect("area_entered", self, "on_hurt")
	ump_feetbox.connect("area_entered", self, "on_ground")
	graph_falling.flags = 0
	graph_calling.flags = 0
	graph_attacking.flags = 0
	graph_ejected.flags = 0
	self.ump.texture = graph_falling
	
	self.ump.frame = 0
	self.state = STATE.Fall
	self.attack_move_speed = self.ATTACK_MOVE_SPEED
	self.fall_speed = self.BASE_FALL_SPEED
	self.frame_delay = self.THROW_DELAY
	set_process(true)

func on_falling_timer():
	throw_furnace()
	self.ump.frame = 1

func on_ground(a):
	#print("0")
	if ump_feetbox.is_connected("area_entered", self, "on_ground"):
		ump_feetbox.disconnect("area_entered", self, "on_ground")
	self.ump.texture = graph_attacking
	self.ump.frame = 0
	self.state = STATE.Idle
	self.frame_delay = self.ATTACK_DELAY

func on_attack_delay_end():
	#self.ump.texture = graph_attacking
	self.ump.frame = 1
	self.state = STATE.Attack
	self.frame_delay = self.ACTIVE_DELAY

func on_attack_timeout():
	self.on_ground(null)


func on_hurt(a):
	if a == screen_borders:
		on_out_of_screen()
	else:
		yield(get_tree(), "idle_frame")
		self.ump_hitbox.set_monitorable(false)
		self.ump_feetbox.set_monitorable(false)
		self.frame_delay = self.HURT_DELAY
		self.ump.texture = graph_ejected
		self.ump.frame = 0
		self.state = STATE.Ejected
		li.ump_hurt_sfx.play(0)


func on_hurt_delay_end():
	#self.ump.texture = graph_ejected
	self.ump.frame = 1

func on_out_of_screen():
	god.combo_gauge += 1
	god.score += 150 * god.combo
	god.combo += 1
	self.call_deferred("queue_free")

func _process(delta):

	if self.state == STATE.Fall:
		refresh_direction()
		self.position += fall_speed * direction
		self.fall_speed += Vector2(0, 0.15)
		if frame_delay == 0:
			on_falling_timer()
	elif self.state == STATE.Idle:
		refresh_direction()
		if frame_delay == 0:
			on_attack_delay_end()
		#animation
		pass
	elif self.state == STATE.Attack:
		self.position += attack_move_speed * direction
		if frame_delay == 0:
			on_attack_timeout()
		pass
	elif self.state == STATE.Ejected:
		refresh_direction()
		self.position += EJECT_SPEED * direction
		if frame_delay == 0:
			on_hurt_delay_end()
	self.frame_delay -= 1
func refresh_direction():
	if li.position.x >= self.position.x:
		#left to right
		self.direction = Vector2(1, 1)
	else:
		#right to left
		self.direction = Vector2(-1, 1)
	self.scale = self.direction
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func throw_furnace():
	remove_child(furnace)
	get_parent().add_child(furnace)
	furnace.position = self.position
	furnace.set_script(self.furnace_script)
	furnace.thrown(self.direction)
	
#	
	
func _draw():
	self.position = Vector2(round(position.x),round(position.y))