extends Node2D

const MAX_HP = 100
const BASE_JUMP_SPEED = Vector2(-0.25, 3)
const EJECT_SPEED = Vector2(-3, -3)
const SLIDE_MOVE_SPEED = Vector2(3,0)
const IFRAME_DELAY = 58
const WALK_DELAY = 3
const RECENTER_SPEED = Vector2(7,0)
const BASE_GROUND_SPEED = Vector2(2, 0)
const GROUND_ACCELERATION = Vector2(1, 0)
const GROUND_SPEED_CAP = Vector2(4.5, 500)
const AIR_ACCELERATION = Vector2(1.5, 0)
const AIR_SPEED_CAP = Vector2(6, 500)
const JUMP_SPEED = Vector2(0, -3.2)
const FRICTION = 0.40
const ANTIAIR_DELAY = 6
const ANTIAIR_ACTIVE_DELAY = 15
const ANTIAIR_SPEED = Vector2(2,-8)
const SLIDE_DELAY = 6
const SLIDE_ACTIVE_DELAY = 12
const SLIDE_GROUND_SPEED = Vector2(15,0)
const SLIDE_AIR_SPEED = Vector2(8,8)
const TEST_JUMP_N_MAX = 7

const GRAVITY = Vector2(0, 0.33)
enum STATE{
	Idle,
	Walk,
	Jump,
	Antiair,
	Slide,
	Hurt,
	Dead}
var hp
var state
var direction
var frame_delay = 0
var onground
var oniframes
var iframe_delay
var fall_speed
var ground_speed = Vector2()
var air_speed = Vector2()
var slide_move_speed
var anti_available
var slide_available
var test_jump_n =0
var test_left_on_next = false
var test_right_on_next = false
onready var screen_borders = god.borderbox
onready var ground = god.groundbox
onready var timer = get_node("Timer")
onready var map_bbox_left = god.bbox_left
onready var map_bbox_right = god.bbox_right
onready var antiair_sfx = get_node("AntiairSFX")
onready var jump_sfx = get_node("JumpSFX")
onready var slide_sfx = get_node("SlideSFX")
onready var hurt_sfx = get_node("HurtSFX")
onready var die_sfx = get_node("DieSFX")
onready var combo_full_sfx = get_node("ComboFullSFX")
onready var ump_hurt_sfx = get_node("UmpHurtSFX")
onready var li = get_node("LiSprite")
onready var li_hitbox = get_node("LiSprite/Hitbox")
onready var li_feetbox = get_node("LiSprite/Feetbox")
onready var antiair = get_node("Antiair")
onready var antiair_hitbox = get_node("Antiair/Hitbox")
onready var slide_hitbox = get_node("LiSprite/SlideHitbox")
onready var fallbox = get_node("LiSprite/Fallbox")
onready var bbox_left = get_node("LiSprite/BoundboxLeft")
onready var bbox_right = get_node("LiSprite/BoundboxRight")
onready var graph_idle = [ preload("res://data/graphics/li_idle.png"), 2 ]
onready var graph_walk = [ preload("res://data/graphics/li_walk.png"), 4 ]
onready var graph_jump = [ preload("res://data/graphics/li_jump.png"), 2 ]
onready var graph_antiair = [ preload("res://data/graphics/li_aa.png"), 2]
onready var graph_slide = [ preload("res://data/graphics/li_slide.png"), 2 ]
onready var graph_hurt = [ preload("res://data/graphics/li_hurt.png"), 2 ]
onready var graph_dead = [ preload("res://data/graphics/li_dead.png"), 2 ]



func _ready():
	li_hitbox.connect("area_entered", self, "on_hurt")
	li_feetbox.connect("area_entered", self, "on_ground")
	li_feetbox.connect("area_exited", self, "on_air")
	bbox_left.connect("area_entered", self, "too_much_on_left")
	bbox_right.connect("area_entered", self, "too_much_on_right")
	fallbox.connect("area_entered", self, "on_die")
	graph_idle[0].flags = 0
	graph_walk[0].flags = 0
	graph_jump[0].flags = 0
	graph_antiair[0].flags = 0
	graph_slide[0].flags = 0
	graph_hurt[0].flags = 0
	graph_dead[0].flags = 0
	self.hp = self.MAX_HP
	self.iframe_delay = 20
	self.oniframes = true
	self.li.texture = graph_idle[0]
	self.li.hframes = graph_idle[1]
	self.li.frame = 0
	self.state = STATE.Idle
	self.slide_move_speed = self.SLIDE_MOVE_SPEED
	self.direction = Vector2(1,1)
	self.ground_speed.x = 0
	self.antiair.visible = false
	set_idle()
	refresh_direction()

func set_idle():
	self.state = STATE.Idle
	self.li.texture = graph_idle[0]
	self.li.hframes = graph_idle[1]
	self.li.frame = 0

func set_walk():
	self.state = STATE.Walk
	self.li.texture = graph_walk[0]
	self.li.hframes = graph_walk[1]
	self.li.frame = 0
	self.frame_delay = self.WALK_DELAY

func set_air_idle():
	self.air_speed.x = self.ground_speed.x
	self.onground = false
	state = STATE.Jump
	self.li.texture = graph_jump[0]
	self.li.hframes = graph_jump[1]
	self.li.frame = 1

func do_antiair():
	self.antiair_sfx.play(0)
	self.state = STATE.Antiair
	self.li.frame = 0
	self.frame_delay = self.ANTIAIR_DELAY
	self.air_speed = Vector2(0,0)
	self.li.texture = graph_antiair[0]
	self.li.hframes = graph_antiair[1]
	self.iframe_delay = 20
	self.oniframes = true

func on_antiair_delay():
	self.li.frame = 1
	self.frame_delay = self.ANTIAIR_ACTIVE_DELAY
	self.antiair.frame = 0
	self.antiair.visible = true
	self.antiair_hitbox.monitorable = true
	self.air_speed = self.ANTIAIR_SPEED
	self.li_feetbox.monitoring = false
	
func on_antiair_timeout():
	self.antiair.visible = false
	self.antiair_hitbox.monitorable = false
	self.li_feetbox.monitoring = true
	if self.onground:
		set_idle()
	else:
		set_air_idle()

func do_slide():
	self.slide_sfx.play(0)
	self.state = STATE.Slide
	self.li.frame = 0
	self.frame_delay = self.SLIDE_DELAY
	self.ground_speed = Vector2(0,0)
	self.air_speed = Vector2(0,0)
	self.iframe_delay = 20
	self.oniframes = true
	self.li.texture = graph_slide[0]
	self.li.hframes = graph_slide[1]

func on_slide_delay():
	self.li.frame = 1
	self.frame_delay = self.SLIDE_ACTIVE_DELAY
	self.slide_hitbox.monitorable = true
	if self.onground:
		self.ground_speed = self.SLIDE_GROUND_SPEED
	else:
		self.air_speed = self.SLIDE_AIR_SPEED
	
func on_slide_timeout():
	self.slide_hitbox.monitorable = false
	if self.onground:
		set_idle()
	else:
		set_air_idle()

func on_ground(a):
	self.anti_available = true
	onground = true
	if state != STATE.Slide:
		set_idle()

func on_air(a):
	onground = false

func too_much_on_left(a):
	self.test_left_on_next = true
	self.direction = Vector2(1,1)
	self.ground_speed.x = self.RECENTER_SPEED.x * self.direction.x
	self.air_speed.x = self.RECENTER_SPEED.x * self.direction.x
	

func too_much_on_right(a):
	self.test_right_on_next = true
	self.direction = Vector2(-1,1)
	self.ground_speed.x = self.RECENTER_SPEED.x * self.direction.x
	self.air_speed.x = self.RECENTER_SPEED.x * self.direction.x
	

func on_attack_timeout():
	set_idle()


func on_hurt(a):
	self.hurt_sfx.play(0)
	self.hp -= 15
	self.li.texture = graph_hurt[0]
	self.li.hframes = graph_hurt[1]
	self.li.frame = 0
	self.state = STATE.Hurt
	self.oniframes = true
	self.iframe_delay = self.IFRAME_DELAY
	self.frame_delay = 4


func on_hurt_delay_end():
	#self.ump.texture = graph_hurt
	self.li.frame = 1
	self.frame_delay = 4
	
func on_die(a=null):
	self.die_sfx.play(0)
	self.set_process(false)
	god.on_game_over()

func _process(delta):
	if self.state != STATE.Slide:
		self.slide_hitbox.monitorable = false
	if self.test_left_on_next:
		if self.bbox_left.overlaps_area(self.map_bbox_left):
			too_much_on_left(null)
		else:
			self.test_left_on_next = false
	if self.test_right_on_next:
		if self.bbox_right.overlaps_area(self.map_bbox_right):
			too_much_on_right(null)
		else:
			self.test_right_on_next = false
	
	if self.hp <= 0:
		on_die()
	self.li_hitbox.monitoring = not oniframes
	if Input.is_action_pressed("action_left") or Input.is_action_pressed("action_right"):
		if self.state == STATE.Idle:
			if Input.is_action_pressed("action_right"):
				self.direction = Vector2(1,1)
			else:
				self.direction = Vector2(-1,1)
			refresh_direction()
			set_walk()
			self.ground_speed.x = self.BASE_GROUND_SPEED.x * self.direction.x
			self.position.x += self.ground_speed.x * self.direction.x
			self.ground_speed.x += self.GROUND_ACCELERATION.x * self.direction.x
			#print(self.ground_speed)
			if self.ground_speed.x >= self.GROUND_SPEED_CAP.x:
				self.ground_speed.x = self.GROUND_SPEED_CAP.x
			if self.ground_speed.x <= -self.GROUND_SPEED_CAP.x:
				self.ground_speed.x = -self.GROUND_SPEED_CAP.x
		if self.state == STATE.Jump:
			if Input.is_action_pressed("action_right"):
				self.direction = Vector2(1,1)
			else:
				self.direction = Vector2(-1,1)
			refresh_direction()
			self.air_speed.x += self.AIR_ACCELERATION.x * self.direction.x
			self.position.x += self.air_speed.x
			if self.air_speed.x >= self.AIR_SPEED_CAP.x:
				self.air_speed.x = self.AIR_SPEED_CAP.x
			if self.air_speed.x <= -self.AIR_SPEED_CAP.x:
				self.air_speed.x = -self.AIR_SPEED_CAP.x
		if self.state == STATE.Walk:
			if Input.is_action_pressed("action_right"):
				self.direction = Vector2(1,1)
			else:
				self.direction = Vector2(-1,1)
			refresh_direction()
			self.ground_speed.x += self.GROUND_ACCELERATION.x * self.direction.x
			self.position.x += self.ground_speed.x
			#print(self.ground_speed)
			if self.ground_speed.x >= self.GROUND_SPEED_CAP.x:
				self.ground_speed.x = self.GROUND_SPEED_CAP.x
			if self.ground_speed.x <= -self.GROUND_SPEED_CAP.x:
				self.ground_speed.x = -self.GROUND_SPEED_CAP.x
	else:
		if self.ground_speed.x != 0 && self.state != STATE.Slide && self.state != STATE.Antiair:
			#print(ground_speed)
			self.ground_speed *= 1-self.FRICTION
			#print(ground_speed)
			if self.ground_speed.x > 0 && self.ground_speed.x <= 0.5:
				self.ground_speed.x = 0
				set_idle()
			if self.ground_speed.x < 0 && self.ground_speed.x >= -0.5:
				self.ground_speed.x = 0
				set_idle()
			#print(ground_speed)
			self.position += self.ground_speed
			
			
	if Input.is_action_just_pressed("action_jump"):
		if self.state == STATE.Idle || self.state == STATE.Walk:
			jump_sfx.play(0)
			self.air_speed.y = self.JUMP_SPEED.y
			set_air_idle()
			self.test_jump_n = self.TEST_JUMP_N_MAX
			self.li_feetbox.monitoring = false
	
	if self.test_jump_n > 0:
		self.test_jump_n -= 1
		if Input.is_action_pressed("action_jump"):
			self.air_speed.y += self.JUMP_SPEED.y/5
		elif self.air_speed.y >= 1:
			self.li_feetbox.monitoring = true
	elif self.test_jump_n == 0 && self.air_speed.y >= 1 && self.onground == false:
		self.li_feetbox.monitoring = true
			
	if Input.is_action_just_pressed("action_antiair"):
		if self.state == STATE.Idle || self.state == STATE.Walk || self.state == STATE.Jump:
			if self.anti_available:
				self.air_speed.y = self.JUMP_SPEED.y
				do_antiair()
				self.anti_available = false
	if Input.is_action_just_pressed("action_slide"):
		if self.state == STATE.Idle || self.state == STATE.Walk || self.state == STATE.Jump:
			if self.slide_available:
				self.air_speed.y = self.JUMP_SPEED.y
				do_slide()
				self.slide_available = false
		
			
	#print(self.position)
	
	self.position = Vector2(floor(self.position.x),floor(self.position.y))
	if self.iframe_delay > 0:
		self.iframe_delay -=1
		if self.iframe_delay <= 0:
			self.oniframes = false
	
	if self.frame_delay > 0:
		self.frame_delay -= 1
	
	if self.state != STATE.Antiair:
		self.antiair.visible = false
		self.antiair_hitbox.monitorable = false
	
	
	
	
	if self.state == STATE.Idle:
		self.slide_available = true
		refresh_direction()
		if onground == false:
			set_air_idle()
	
	elif self.state == STATE.Walk:
		if self.frame_delay == 0:
			self.frame_delay = self.WALK_DELAY
			
			if self.li.frame + 1 == graph_walk[1]:
				self.li.frame = 0
			else:
				self.li.frame += 1
		self.slide_available = true
		refresh_direction()
		if onground == false:
			set_air_idle()
	
	elif self.state == STATE.Jump:
		self.air_speed.y += self.GRAVITY.y
		self.position.y += self.air_speed.y
		if self.li_feetbox.monitoring == true:
			for i in god.get_three_closest_block(position.x):
				if i != null:
					if self.li_feetbox.overlaps_area(i):
						on_ground()
		refresh_direction()
		
	elif self.state == STATE.Slide:
		if self.onground:
			self.position += self.ground_speed * self.direction
			self.ground_speed *= 1 - (self.FRICTION/5.0)
		else:
			self.position += self.air_speed * self.direction
			self.air_speed *= 1 - (self.FRICTION/5.0)
		if self.li.frame == 0 && self.frame_delay == 0:
			on_slide_delay()
		if self.li.frame == 1 && self.frame_delay == 0:
			on_slide_timeout()
		refresh_direction()
		
	elif self.state == STATE.Antiair:
		self.position += self.air_speed * self.direction
		self.air_speed *= 1 - (self.FRICTION/2)
		if self.li.frame == 0 && self.frame_delay == 0:
			on_antiair_delay()
		if self.li.frame == 1 && self.frame_delay == 0:
			on_antiair_timeout()
		refresh_direction()
		
	elif self.state == STATE.Hurt:
		self.position += self.EJECT_SPEED * self.direction
		if self.frame_delay == 0 && self.li.frame == 0:
			on_hurt_delay_end()
		if self.frame_delay == 0 && self.li.frame == 1:
			self.state = STATE.Idle
			ground_speed.x = self.EJECT_SPEED.x * self.direction.x
		
	elif self.state == STATE.Dead:
		pass

func refresh_direction():
	self.scale = self.direction

#	pass

