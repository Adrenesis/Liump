extends Sprite
const MIDDLE_SCALE = Vector2(1.25, 1.25)
const BIG_SCALE = Vector2(1.7, 1.7)
const ANIM_DELAY = 2
const MAX_FRAME = 13
onready var sfx = get_node("AudioStreamPlayer")
onready var heatbox = get_node("Heatbox")
onready var hitbox = get_node("Hitbox")
var anim_delay
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	self.anim_delay = self.ANIM_DELAY
	self.texture.flags = 0
	# Called when the node is added to the scene for the first time.
	# Initialization here
	sfx.play(0)
	pass

func _process(delta):
	if self.anim_delay <= 0:
		if self.frame == 13:
			self.queue_free()
		else:
			self.frame +=1
			self.anim_delay = self.ANIM_DELAY
	else:
		self.anim_delay -= 1
	if self.frame >=2 && self.frame <= 3:
		self.heatbox.scale = self.MIDDLE_SCALE
		self.hitbox.scale = self.MIDDLE_SCALE
	if self.frame >=4 && self.frame <= 5:
		self.heatbox.scale = self.BIG_SCALE
		self.hitbox.scale = self.BIG_SCALE
	if self.frame >=6 && self.frame <= 7:
		self.heatbox.scale = self.BIG_SCALE
		self.hitbox.scale = self.BIG_SCALE
	if self.frame > 7:
		self.hitbox.monitorable = false
		self.heatbox.monitorable = false
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass
