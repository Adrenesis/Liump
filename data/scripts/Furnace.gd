extends Sprite
const GRAVITY = Vector2(1, 2)
const SPEED_CAP = Vector2(3, 5)
const EJECT_SPEED = Vector2(-12, -6)
const BLOW_UP_DELAY = 70
const ANIM_DELAY = 4
const HURT_DELAY = 2
onready var li = god.li
onready var explosion = preload("res://data/scenes/characters/Explosion.tscn")
onready var graph_fall = preload("res://data/graphics/furnace_fall.png")
onready var graph_blow_up = preload("res://data/graphics/furnace_blow_up.png") 
onready var hitbox = get_node("Hitbox")
onready var feetbox = get_node("Feetbox")
onready var heatbox = get_node("Heatbox")
enum STATE{
	Held,
	Thrown,
	BlowingUp,
	Ejected
}
var state
var speed = Vector2(0,0)
var frame_delay
var anim_delay
var screen_borders
var direction
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	self.screen_borders = god.borderbox
	state =	STATE.Held
	set_process(false)
	self.graph_fall.flags = 0
	self.graph_blow_up.flags = 0
	self.frame_delay = -1
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func thrown(d):
	self.state = STATE.Thrown
	self.direction = d
	self.frame = 1
	self.position.x += 20
	self.position.y -= 10
	hitbox.connect("area_entered", self, "on_hurt")
	feetbox.connect("area_entered", self, "on_ground")
	set_process(true)
	
func on_hurt(a):
	if a == screen_borders:
		on_out_of_screen()
	else:
		li.ump_hurt_sfx.play(0)
		yield(get_tree(), "idle_frame")
		self.hitbox.set_monitorable( false)
		self.feetbox.set_monitoring(false)
		self.heatbox.set_monitorable(false)
		self.frame_delay = self.HURT_DELAY
		self.modulate = Color(1, 0, 0.2, 0.33)
		self.frame = 0
		self.state = STATE.Ejected
	pass

func on_out_of_screen():
	god.score += 200 * god.combo
	god.combo += 1
	god.combo_gauge += 1
	self.call_deferred("queue_free")

func on_ground(a):
	self.state = STATE.BlowingUp
	self.anim_delay = self.ANIM_DELAY
	self.texture = self.graph_blow_up
	self.frame_delay = self.BLOW_UP_DELAY
	pass

func blow_up():
	god.general_temperature += 1
	god.combo_gauge = 0
	god.combo = 0
	var expl = explosion.instance()
	get_parent().add_child(expl)
	expl.position = self.position
	self.queue_free()
	pass

func _draw():
	self.texture.flags = 0
	self.position = Vector2(self.position.x, self.position.y)
func _process(delta):
	if self.state == STATE.BlowingUp:
		if self.frame_delay > 0:
			self.frame_delay -= 1
			self.anim_delay -= 1
			if self.anim_delay <= 0:
				self.anim_delay = self.ANIM_DELAY
				if self.frame == 0:
					self.frame += 1
				else:
					self.frame -= 1
		elif self.frame_delay == 0:
			blow_up()
	elif self.state == STATE.Thrown:
		self.speed += self.GRAVITY * self.direction
		if self.speed >= self.SPEED_CAP:
			self.speed = self.SPEED_CAP
		if self.speed <= -self.SPEED_CAP:
			self.speed = Vector2(-self.SPEED_CAP.x, self.SPEED_CAP.y)
		self.position += self.speed
	elif self.state == STATE.Ejected:
		self.position += self.EJECT_SPEED
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
